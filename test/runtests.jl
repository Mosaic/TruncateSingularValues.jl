using Test
using TruncateSingularValues
using LinearAlgebra
using Random

# initialize random values
Random.seed!(1985399162591197402)

# Generate low rank data
t = range(-3, 3, 601)
U = [@.(cos(17*t)*exp(-t^2)) @.(sin(11*t))]
S = diagm([2, 0.5])
V = [@.(sin(5*t)*exp(-t^2)) @.(cos(13*t))]
X = U*S*V'

# Contaminate signal with noise
σ = 1
Xₙ = X + σ * randn(size(X))

# Compute standard economy svd
Uₙ, Sₙ, Vₙ = svd(Xₙ)

# Cutoff at 90%
i = findall(Σ -> Σ < 0.9, cumsum(Sₙ)/sum(Sₙ))
X̃ = Uₙ[:,i]*diagm(Sₙ[i])*Vₙ[:,i]'

# Compute relative difference
δX̃ = norm(X - X̃)/norm(X)

# Truncate using optimal hard threshold with known noise
Ū, S̄, V̄ = tsvd(Xₙ, σ)

# Reconstruct data
X̄ = Ū*diagm(S̄)*V̄'

# Compute relative difference to original data
δX̄ = norm(X - X̄)/norm(X)

@test δX̄ < 1 < δX̃

# Truncate using optimal hard threshold *without known noise
Ū, S̄, V̄ = tsvd(Xₙ)

# Reconstruct data
X̄ = Ū*diagm(S̄)*V̄'

# Compute relative difference to original data
δX̄ = norm(X - X̄)/norm(X)

@test δX̄ < 1 < δX̃