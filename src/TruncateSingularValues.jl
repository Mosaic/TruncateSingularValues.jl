module TruncateSingularValues

# External dependencies

using LinearAlgebra: SVD, svd
using Statistics: median

# Implementation

export SVD, tsvd

"Perform a truncated SVD using hard threshold as provided by Gavish et al. (DOI: 0.1109/TIT.2014.2323359)"
function tsvd(A, γ=nothing; kwargs...)
	# compute svd
	F = svd(A; kwargs...)
	# get height of matrix
	n = size(A, 1)
	# compute aspect ratio of matrix
	β = aspectratio(size(A)...)
	# check if noise magnitude is known
	τ = (isnothing(γ)
		# compute approximate hard threshold using median singular value
		? ( ω(β) * median(F.S) )
		# is matrix square or rectangular?
		: ( (isone(β) ? 4/√3 : λ(β)) * √n * γ ))
	# find all indices which are below threshold
	i = findall(σ -> σ > τ, F.S)
	# i = searchsorted(F.S, τ; lt=<, rev=true)
	# truncate svd
	SVD(F.U[:,i], F.S[i], F.Vt[i,:])
end

"Compute aspect ratio of rectangular matrix s.t. `β ∈ (0, 1)`."
aspectratio(n, m) = (n>m) * m/n + (m>n) * n/m + (n==m)

λ(β) = √( 2(β+1) + 8β/( (β+1)+√(β*β+14β+1) ) )

ω(β) = λ(β) / μᵦ(β)

"Numerically approximate where cumulative Marchenko–Pastur distribution is half."
function μᵦ(β)
	# average as start value
	x = 0.5((1-√β)^2+(1+√β)^2)
	# initialize comparison
	x̃ = 0
	# iterate until current and previous solution are numerically equivalent
	while !(x̃ ≈ x)
		# Newton method is stable enough that the absolut value may be taken
		# for the degenerate case of `β = 1` first step
		x̃, x = x, abs(x - (cmpd(x, β) - 0.5)/mpd(x, β))
	end
	# return the solution
	x
end

"Compute Marchenko–Pastur distribution."
mpd(t, β) = sqrt( ((1+√β)^2-t) * (t-(1-√β)^2) ) / (2π*t*β)

"Compute cumulative Marchenko–Pastur distribution."
function cmpd(t::T, β) where T
	# compute left bound
	β₋ = (1-√β)^2
	# early terminate if `t ∈ (-∞, β₋]`
	t ≤ β₋ && return zero(T)
	# compute right bound
	β₊ = (1+√β)^2
	# compute helper variables
	r² = (β₊-t)/(t-β₋)
	r  = √r²
	# compute integral of Marchenko–Pastur distribution
	0.5 + (sqrt((β₊-t)*(t-β₋)) - (1+β)*atan(0.5(r²-1)/r) + (1-β)*atan(0.5(r²*β₋-β₊)/((1-β)*r))) / (2π*β)
end

end # TruncateSingularValues